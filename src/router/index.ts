import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },
    {
      path: "/food-list",
      name: "Food List",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/foodlist/FoodListView.vue"),
    },
    {
      path: "/table",
      name: "Table Menu",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/TableMenuView.vue"),
    },
    {
      path: "/QRcode",
      name: "QRcode",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/QRcodeView.vue"),
    },
    {
      path: "/CheckInOut",
      name: "CheckInOut",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // @ts-ignore
      component: () => import("../views/CheckInOutView.vue"),
    },
    {
      path: "/CancleOrder",
      name: "CancleOrder",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/CancleOrderView.vue"),
    },
    {
      path: "/OrderCustomer",
      name: "OrderCustomer",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/OrderView.vue"),
    },
    {
      path: "/OrderTable",
      name: "OrderTable",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/OrderTableView.vue"),
    },
    {
      path: "/CheckBillChoice",
      name: "CheckBillChoice",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/CheckBillChoiceView.vue"),
    },
    {
      path: "/QRcodeBill",
      name: "QRcodeBill",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/QRcodeBillView.vue"),
    },
    {
      path: "/user",
      name: "user",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/users/UserView.vue"),
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: "/login",
      name: "login",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/LoginView.vue"),
    },
    {
      path: "/food",
      name: "food",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/food/FoodView.vue"),
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: "/food-serve",
      name: "foodserve",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/foodlist/FoodServeView.vue"),
    },
    {
      path: "/employee",
      name: "employee",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/employee/EmpView.vue"),
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: "/stock",
      name: "stock",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/stock/StockView.vue"),
      // meta: {
      //   requiresAuth: true,
      // },
    },
    {
      path: "/OrderedFood",
      name: "OrderedFood",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/OrderedFoodView.vue"),
    },
  ],
});

function isLogin() {
  const user = localStorage.getItem("user");
  if (user) {
    return true;
  }
  return false;
}

router.beforeEach((to, from) => {
  // instead of having to check every route record with
  // to.matched.some(record => record.meta.requiresAuth)
  if (to.meta.requiresAuth && !isLogin()) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    return {
      path: "/",
      // save the location we were at to come back later
      // query: { redirect: to.fullPath },
    };
  }
});

export default router;
