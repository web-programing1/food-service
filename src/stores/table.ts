import { ref } from "vue";
import { defineStore } from "pinia";
import table from "@/services/table";
import type Table from "@/types/Table";
//import { useMessageStore } from "./message";

export const useTableStore = defineStore("table", () => {
  //const messageStore = useMessageStore();
  const selected = ref<Table>({});
  const dialog = ref(false);
  const tableList = ref<Table[]>([]);
  //secondary=ว่าง error=ไม่ว่าง orange=ทำความสะอาด

  async function getTables() {
    try {
      const res = await table.getTables();
      tableList.value = res.data;
    } catch (e) {
      console.log(e);
      //messageStore.showMassage("Can't get table information from database.");
    }
  }
  const selectTables = (table: Table): void => {
    selected.value = table;
    console.log(selected.value);
  };
  const updateTables = async (status: string) => {
    try {
      selected.value.status = status;
      const res = await table.updateTables(selected.value);
      console.log(res.data);
    } catch (e) {
      console.log(e);
    }
    dialog.value = false;
    await getTables();
  };
  const confirmClear = async (): Promise<void> => {
    try {
      selected.value.status = "A";
      const res = await table.updateTables(selected.value);
      console.log(res.data);
    } catch (e) {
      console.log(e);
    }
    dialog.value = false;
    await getTables();
  };
  const cancle = (): void => {
    dialog.value = false;
    selected.value = {};
  };
  return {
    tableList,
    getTables,
    selectTables,
    updateTables,
    confirmClear,
    cancle,
    selected,
    dialog,
  };
});
