import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type FoodList from "@/types/FoodList";
import foodListService from "@/services/foodlist";
import foodService from "@/services/food";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Food from "@/types/Food";
import type ServeFood from "@/types/ServeFood";
import foodlist from "@/services/foodlist";

export const useFoodListStore = defineStore("FoodList", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const foodLists = ref<FoodList[]>([]);
  const editedFoodLists = ref<FoodList>({ orderStatus: "" });
  const food = ref<Food>();
  const serveFoodLists = ref<ServeFood[]>([]);

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedFoodLists.value = { orderStatus: "" };
    }
  });

  async function getFoodFromId(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await foodService.getFoodFromId(id);
      food.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Food ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getServeFood() {
    loadingStore.isLoading = true;
    try {
      const res = await foodListService.getServeFood();
      serveFoodLists.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล FoodList ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getFoodListFromId(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await foodListService.getFoodsListFromId(id);
      editedFoodLists.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Food ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getFoodsList() {
    loadingStore.isLoading = true;
    try {
      const res = await foodListService.getFoodsList();
      foodLists.value = res.data;
      getServeFood();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล FoodList ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveFoodList() {
    loadingStore.isLoading = true;
    try {
      if (editedFoodLists.value.id) {
        const res = await foodListService.updateFoodList(
          editedFoodLists.value.id,
          editedFoodLists.value
        );
      } else {
        const res = await foodListService.saveFoodList(editedFoodLists.value);
      }

      dialog.value = false;
      await getFoodsList();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก FoodList ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function updateStatus(id: number, status: string) {
    loadingStore.isLoading = true;
    try {
      await getFoodListFromId(id);
      editedFoodLists.value.id = id;
      editedFoodLists.value.orderStatus = status;
      const res = await foodListService.updateFoodList(
        editedFoodLists.value.id,
        editedFoodLists.value
      );
      await getFoodsList();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก FoodList ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteFoodList(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await foodListService.deleteFoodList(id);
      await getFoodsList();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ FoodList ได้");
    }
    loadingStore.isLoading = false;
  }
  function editFood(FoodList: FoodList) {
    editedFoodLists.value = JSON.parse(JSON.stringify(FoodList));
    dialog.value = true;
  }
  return {
    foodLists,
    getFoodsList,
    dialog,
    editedFoodLists,
    saveFoodList,
    editFood,
    deleteFoodList,
    updateStatus,
    getServeFood,
    serveFoodLists,
  };
});
