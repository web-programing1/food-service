import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Order from "@/types/Order";
import orderService from "@/services/order";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import food from "@/services/food";
import type BillItem from "@/types/BillItem";
import { useTableStore } from "./table";

export const useOrderStore = defineStore("Order", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const tableStore = useTableStore();
  const dialog = ref(false);
  const orders = ref<Order[]>([]);
  const editedOrder = ref<Order>({
    orderItems: [],
    table: 0,
    status: "pending",
  });
  const bill = ref<BillItem[]>([]);

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedOrder.value = {
        orderItems: [],
        table: 0,
        status: "pending",
      };
    }
  });

  async function changeOrderStatusToPaidFromTableId(tableId: number) {
    loadingStore.isLoading = true;
    try {
      await orderService.deleteOrderFromTableId(tableId);
    } catch (error) {
      messageStore.showError("ไม่สามารถลบข้อมูล Order ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getBillFromTableId(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.getBillOrderFromTableId(id);
      bill.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Bill ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getOrderFromId(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.getOrderFromId(id);
      orders.value = res.data;
      return orders.value;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Order ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getOrderFromTableId(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.getOrderFromTableId(id);
      orders.value = res.data;
      return orders.value;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Order ได้");
    }
    loadingStore.isLoading = false;
  }

  async function changeOrderTableId(oldTableId: number, newTableId: number) {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.getOrderFromTableId(oldTableId);
      orders.value = res.data;
      orders.value.forEach((order) => {
        order.table = newTableId;
        updateOrder(order);
      });
      return orders.value;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Order ได้");
    }
    loadingStore.isLoading = false;
  }

  async function updateOrder(order: Order) {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.updateOrder(order.id!, order);
      orders.value = res.data;
    } catch (error) {
      messageStore.showError("ไม่สามารถอัพเดทข้อมูล Order ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getOrders() {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.getOrders();
      orders.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Order ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveOrder() {
    loadingStore.isLoading = true;
    try {
      if (editedOrder.value.id) {
        console.log("1");
        const res = await orderService.updateOrder(
          editedOrder.value.id,
          editedOrder.value
        );
      } else {
        console.log("2");
        const res = await orderService.saveOrder(editedOrder.value);
      }
      editedOrder.value.orderItems = [];
      await getOrders();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Order ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteOrder(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.deleteOrder(id);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Order ได้");
    }
    loadingStore.isLoading = false;
  }

  function editOrder(Order: Order) {
    editedOrder.value = JSON.parse(JSON.stringify(Order));
    dialog.value = true;
  }

  async function checkBill(id: number) {
    loadingStore.isLoading = true;
    try {
      const order = await orderService.getOrderFromTableId(id);
      console.log(order.data.length);
      // for (let i = 0; i < order.data.length; i++) {
      //   await orderService.deleteOrder(order.data[i].id);
      //   order.data[i].status = "success";
      // }
      order.data.forEach(async function (item: any) {
        item.status = "success";
        updateOrder(item);
        await orderService.deleteOrder(item.id);
      });
      // const res = await orderService.deleteOrder(order.data);
      tableStore.updateTables("C");
      await getOrders();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถชำระเงินได้");
    }
    loadingStore.isLoading = false;
  }
  return {
    orders,
    getOrders,
    dialog,
    editedOrder,
    saveOrder,
    editOrder,
    deleteOrder,
    getOrderFromId,
    getOrderFromTableId,
    getBillFromTableId,
    bill,
    changeOrderTableId,
    checkBill,
    changeOrderStatusToPaidFromTableId,
  };
});
