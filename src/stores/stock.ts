import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";

import stockService from "@/services/stock";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Stock from "@/types/Stock";
import stock from "@/services/stock";

export const useStockStore = defineStore("Stock", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const stocks = ref<Stock[]>([]);
  const editedStock = ref<Stock>({
    name: "",
    amount: 0,
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedStock.value = {
        name: "",
        amount: 0,
      };
    }
  });
  async function getStocks() {
    loadingStore.isLoading = true;
    try {
      const res = await stockService.getStocks();
      stocks.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Stock ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveStock() {
    loadingStore.isLoading = true;
    try {
      if (editedStock.value.id) {
        const res = await stockService.updateStock(
          editedStock.value.id,
          editedStock.value
        );
      } else {
        const res = await stockService.saveStock(editedStock.value);
      }

      dialog.value = false;
      await getStocks();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Stock ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteStock(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await stockService.deleteStock(id);
      await getStocks();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Stork ได้");
    }
    loadingStore.isLoading = false;
  }
  function editStock(stock: Stock) {
    editedStock.value = JSON.parse(JSON.stringify(stock));
    dialog.value = true;
  }
  return {
    stocks,
    getStocks,
    dialog,
    editedStock,
    saveStock,
    editStock,
    deleteStock,
  };
});
