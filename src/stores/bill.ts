import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Bill from "@/types/Bill";
import billService from "@/services/bill";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import { useTableStore } from "./table";
import bill from "@/services/bill";
import type BillItem from "@/types/BillItem";
import type Report from "@/types/Report";
import type FoodReport from "@/types/FoodReport";

export const useBillStore = defineStore("Bill", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const tableStore = useTableStore();
  const bills = ref<Bill[]>([]);
  const reports = ref<Report[]>([]);
  const foodReports = ref<FoodReport[]>([]);
  const foodReport = ref<FoodReport>();
  const report = ref<Report>({
    date: "0",
    numBills: 0,
    totalSales: 0,
    totalAmount: 0,
  });
  const editedBill = ref<Bill>({
    billItems: [],
    totalPrice: 0,
  });

  async function payBill(tempBillItems: BillItem[]) {
    loadingStore.isLoading = true;
    try {
      editedBill.value.billItems = tempBillItems;
      const res = await billService.saveBill(editedBill.value);
      tableStore.updateTables("C");
    } catch (error) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Bill ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getBillFromId(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await billService.getBillFromId(id);
      bills.value = res.data;
      return bills.value;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Bill ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getDayFoodBills() {
    loadingStore.isLoading = true;
    try {
      const res = await billService.getDayFoodBills();
      foodReports.value = res.data;
      foodReport.value = foodReports.value[0];
    } catch (error) {
      console.log(error);
      messageStore.showError("ไม่สามารถดึงข้อมูล Bill ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getMonthFoodBills() {
    loadingStore.isLoading = true;
    try {
      const res = await billService.getMonthFoodBills();
      foodReports.value = res.data;
      foodReport.value = foodReports.value[0];
    } catch (error) {
      console.log(error);
      messageStore.showError("ไม่สามารถดึงข้อมูล Bill ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getYearFoodBills() {
    loadingStore.isLoading = true;
    try {
      const res = await billService.getYearFoodBills();
      foodReports.value = res.data;
      foodReport.value = foodReports.value[0];
    } catch (error) {
      console.log(error);
      messageStore.showError("ไม่สามารถดึงข้อมูล Bill ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getDayBills() {
    loadingStore.isLoading = true;
    try {
      const res = await billService.getDayBills();
      reports.value = res.data;
      report.value = reports.value[0];
    } catch (error) {
      console.log(error);
      messageStore.showError("ไม่สามารถดึงข้อมูล Bill ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getMonthBills() {
    loadingStore.isLoading = true;
    try {
      const res = await billService.getMonthBills();
      reports.value = res.data;
      report.value = reports.value[0];
    } catch (error) {
      console.log(error);
      messageStore.showError("ไม่สามารถดึงข้อมูล Bill ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getYearBills() {
    loadingStore.isLoading = true;
    try {
      const res = await billService.getYearBills();
      reports.value = res.data;
      report.value = reports.value[0];
    } catch (error) {
      console.log(error);
      messageStore.showError("ไม่สามารถดึงข้อมูล Bill ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getBills() {
    loadingStore.isLoading = true;
    try {
      const res = await billService.getBills();
      bills.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Bill ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveBill() {
    loadingStore.isLoading = true;
    try {
      if (editedBill.value.id) {
        const res = await billService.updateBill(
          editedBill.value.id,
          editedBill.value
        );
      } else {
        const res = await billService.saveBill(editedBill.value);
      }

      await getBills();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Bill ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteBill(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await billService.deleteBill(id);
      await getBills();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Bill ได้");
    }
    loadingStore.isLoading = false;
  }
  function editBill(Bill: Bill) {
    editedBill.value = JSON.parse(JSON.stringify(Bill));
  }
  return {
    bills,
    getBills,
    editedBill,
    saveBill,
    editBill,
    deleteBill,
    getBillFromId,
    payBill,
    getDayBills,
    getMonthBills,
    getYearBills,
    report,
    reports,
    getDayFoodBills,
    getMonthFoodBills,
    getYearFoodBills,
    foodReport,
    foodReports,
  };
});
