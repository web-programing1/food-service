import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Food from "@/types/Food";
import foodService from "@/services/food";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import food from "@/services/food";

export const useFoodStore = defineStore("Food", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const foods = ref<Food[]>([]);
  const editedFood = ref<Food>({
    name: "",
    price: 0,
    ingredients: "",
    calories: 0,
    preparationTime: 0,
    category: "",
    img: "",
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedFood.value = {
        name: "",
        price: 0,
        ingredients: "",
        calories: 0,
        preparationTime: 0,
        category: "",
        img: "",
      };
    }
  });

  async function getFoodFromId(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await foodService.getFoodFromId(id);
      foods.value = res.data;
      return foods.value;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Food ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getFoods() {
    loadingStore.isLoading = true;
    try {
      const res = await foodService.getFoods();
      foods.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Food ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveFood() {
    loadingStore.isLoading = true;
    try {
      if (editedFood.value.id) {
        const res = await foodService.updateFood(
          editedFood.value.id,
          editedFood.value
        );
      } else {
        const res = await foodService.saveFood(editedFood.value);
      }

      dialog.value = false;
      await getFoods();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Food ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteFood(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await foodService.deleteFood(id);
      await getFoods();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Food ได้");
    }
    loadingStore.isLoading = false;
  }
  function editFood(Food: Food) {
    editedFood.value = JSON.parse(JSON.stringify(Food));
    dialog.value = true;
  }
  return {
    foods,
    getFoods,
    dialog,
    editedFood,
    saveFood,
    editFood,
    deleteFood,
    getFoodFromId,
  };
});
