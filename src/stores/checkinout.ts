import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Checkinout from "@/types/Checkinout";
import checkinoutService from "@/services/checkinout";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import { useAuthStore } from "./auth";

export const useCheckinoutStore = defineStore("Checkinout", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const authStore = useAuthStore();
  const checkinouts = ref<Checkinout[]>([]);
  const editedCheckinout = ref<Checkinout>({
    employeeId: 0,
  });
  const localEmail = JSON.parse(localStorage.getItem("user")!).email;

  async function getCheckinout() {
    loadingStore.isLoading = true;
    try {
      const res = await checkinoutService.getCheckinout();
      checkinouts.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล CheckInOut ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getCheckinoutByEmployeeId(employeeId: number) {
    loadingStore.isLoading = true;
    try {
      const res = await checkinoutService.getCheckinoutByEmployeeId(employeeId);
      checkinouts.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล CheckInOut ได้");
    }
    loadingStore.isLoading = false;
  }

  async function checkIn(password: string): Promise<boolean> {
    loadingStore.isLoading = true;
    try {
      const employeeId = await authStore.getEmployeeId(localEmail, password);
      if (employeeId < 0) {
        messageStore.showError("Password ไม่ถูกต้อง");
        loadingStore.isLoading = false;
        return false;
      }
      editedCheckinout.value.employeeId = employeeId;
      console.log(employeeId);
      const res = await checkinoutService.checkIn(editedCheckinout.value);
      console.log(res);
      messageStore.showInfo("CheckIn สำเร็จ");
      // await getCheckinout();
    } catch (e) {
      console.log(e);
      messageStore.showError("Password ไม่ถูกต้อง");
      return false;
    }
    loadingStore.isLoading = false;
    return true;
  }

  async function checkOut(password: string): Promise<boolean> {
    loadingStore.isLoading = true;
    try {
      const employeeId = await authStore.getEmployeeId(localEmail, password);
      if (employeeId < 0) {
        messageStore.showError("Password ไม่ถูกต้อง");
        loadingStore.isLoading = false;
        return false;
      }
      console.log(employeeId);
      const res = await checkinoutService.checkOut(employeeId);
      console.log(res);
      messageStore.showInfo("CheckOut สำเร็จ");
      // await getCheckinout();
    } catch (e) {
      console.log(e);
      messageStore.showError("Password ไม่ถูกต้อง");
      return false;
    }
    loadingStore.isLoading = false;
    return true;
  }

  return {
    getCheckinout,
    getCheckinoutByEmployeeId,
    checkinouts,
    checkIn,
    checkOut,
  };
});
