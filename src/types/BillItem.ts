export default interface BillItem {
  id?: number;
  name: string;
  amount: number;
  total: number;
}
