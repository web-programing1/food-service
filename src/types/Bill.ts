import type BillItem from "./BillItem";

export default interface Bill {
  id?: number;
  billItems: BillItem[];
  totalPrice: number;
}
