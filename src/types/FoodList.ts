import type Food from "./Food";

export default interface FoodList {
  id?: number;
  orderStatus: string;
  food?: Food;
}
