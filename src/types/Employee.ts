export default interface Employee {
  id?: number;
  name: string;
  salary: number;
  workingHours: number;
  job: string;
  phoneNumber: string;
}
