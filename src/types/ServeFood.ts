export default interface ServeFood {
  id: number;
  name: string;
  tableId: number;
}
