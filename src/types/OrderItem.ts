export default interface OrderItem {
  id?: number;
  foodId: number;
  amount: number;
}
