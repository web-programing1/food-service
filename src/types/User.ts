export default interface User {
  id?: number;
  employeeId: number;
  name: string;
  email: string;
  password: string;
  role: string;
}
