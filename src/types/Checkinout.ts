export default interface Checkinout {
  id?: number;
  employeeId: number;
  name?: string;
  checkInTime?: Date;
  checkOutTime?: Date;
}
