import type OrderItem from "./OrderItem";

export default interface Order {
  id?: number;
  orderItems: OrderItem[];
  table: number;
  status: string;
}
