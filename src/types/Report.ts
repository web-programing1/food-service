export default interface Report {
  date: string;
  numBills: number;
  totalSales: number;
  totalAmount: number;
}
