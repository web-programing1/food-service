export default interface Report {
  name: string;
  totalAmount: number;
}
