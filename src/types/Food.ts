export default interface Food {
  id?: number;
  name: string;
  price: number;
  ingredients: string;
  calories: number;
  preparationTime: number;
  category: string;
  img: string;
}
