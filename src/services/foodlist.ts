import type FoodList from "@/types/FoodList";
import http from "./axios";
function getFoodsList() {
  return http.get("/foodlist");
}

function getFoodsListFromId(id: number) {
  return http.get(`/foodlist/${id}`);
}

function getServeFood() {
  return http.get(`/foodlist/serve`);
}

function saveFoodList(foodlist: FoodList) {
  return http.post("/foodlist", foodlist);
}

function updateFoodList(id: number, foodlist: FoodList) {
  return http.patch(`/foodlist/${id}`, foodlist);
}

function deleteFoodList(id: number) {
  return http.delete(`/foodlist/${id}`);
}

export default {
  getFoodsList,
  saveFoodList,
  updateFoodList,
  deleteFoodList,
  getFoodsListFromId,
  getServeFood,
};
