import type Checkinout from "@/types/Checkinout";
import http from "./axios";
function getCheckinout() {
  return http.get("/checkinout");
}

function getCheckinoutByEmployeeId(employeeId: number) {
  return http.get("/checkinout?employeeId=" + employeeId);
}

function checkIn(checkinout: Checkinout) {
  return http.post("/checkinout", checkinout);
}

function checkOut(employeeId: number) {
  return http.delete(`/checkinout/${employeeId}`);
}

export default { getCheckinout, getCheckinoutByEmployeeId, checkIn, checkOut };
