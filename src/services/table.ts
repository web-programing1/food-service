import type Table from "@/types/Table";
import http from "./axios";

function getTables() {
  return http.get("/table");
}
function saveTables(table: Table) {
  return http.post("/table", table);
}
function updateTables(table: Table) {
  return http.patch(`/table/${table.id}`, table);
}
function deleteTables(id: number) {
  return http.delete(`/table/${id}`);
}

export default { getTables, saveTables, updateTables, deleteTables };
