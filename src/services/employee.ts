import type Employee from "@/types/Employee";
import http from "./axios";
function getEmployees() {
  return http.get("/employee");
}

function saveEmployee(employee: Employee) {
  return http.post("/employee", employee);
}

function updateEmployee(id: number, employee: Employee) {
  return http.patch(`/employee/${id}`, employee);
}

function deleteEmployee(id: number) {
  return http.delete(`/employee/${id}`);
}

export default { getEmployees, saveEmployee, updateEmployee, deleteEmployee };
