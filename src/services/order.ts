import type Order from "@/types/Order";
import http from "./axios";
function getOrders() {
  return http.get("/order");
}

function saveOrder(order: Order) {
  return http.post("/order", order);
}

function updateOrder(id: number, order: Order) {
  return http.patch(`/order/${id}`, order);
}

function deleteOrder(id: number) {
  return http.delete(`/order/${id}`);
}

function deleteOrderFromTableId(tableId: number) {
  return http.patch(`/order?tableId=${tableId}`);
}

function getOrderFromId(id: number) {
  return http.get(`/order/${id}`);
}

function getOrderFromTableId(tableId: number) {
  return http.get(`/order?tableId=${tableId}`);
}

function getBillOrderFromTableId(tableId: number) {
  return http.get(`/order?tableBillId=${tableId}`);
}

export default {
  getOrders,
  saveOrder,
  updateOrder,
  deleteOrder,
  getOrderFromId,
  getOrderFromTableId,
  getBillOrderFromTableId,
  deleteOrderFromTableId,
};
