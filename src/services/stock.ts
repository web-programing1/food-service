import type Stock from "@/types/Stock";
import http from "./axios";
function getStocks() {
  return http.get("/stock");
}

function saveStock(stock: Stock) {
  return http.post("/stock", stock);
}

function updateStock(id: number, stock: Stock) {
  return http.patch(`/stock/${id}`, stock);
}

function deleteStock(id: number) {
  return http.delete(`/stock/${id}`);
}

export default { getStocks, saveStock, updateStock, deleteStock };
