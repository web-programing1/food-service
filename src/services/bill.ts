import type Bill from "@/types/Bill";
import http from "./axios";
function getBills() {
  return http.get("/bill");
}

function getDayBills() {
  return http.get("/bill?day=1");
}

function getMonthBills() {
  return http.get("/bill?month=1");
}

function getYearBills() {
  return http.get("/bill?year=1");
}

function getDayFoodBills() {
  return http.get("/bill?food=1&day=1");
}

function getMonthFoodBills() {
  return http.get("/bill?food=1&month=1");
}

function getYearFoodBills() {
  return http.get("/bill?food=1&year=1");
}

function saveBill(bill: Bill) {
  return http.post("/bill", bill);
}

function updateBill(id: number, bill: Bill) {
  return http.patch(`/bill/${id}`, bill);
}

function deleteBill(id: number) {
  return http.delete(`/bill/${id}`);
}

function getBillFromId(id: number) {
  return http.get(`/bill/${id}`);
}

export default {
  getBills,
  saveBill,
  updateBill,
  deleteBill,
  getBillFromId,
  getDayBills,
  getMonthBills,
  getYearBills,
  getDayFoodBills,
  getMonthFoodBills,
  getYearFoodBills,
};
