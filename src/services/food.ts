import type Food from "@/types/Food";
import http from "./axios";
function getFoods() {
  return http.get("/food");
}

function saveFood(food: Food) {
  return http.post("/food", food);
}

function updateFood(id: number, food: Food) {
  return http.patch(`/food/${id}`, food);
}

function deleteFood(id: number) {
  return http.delete(`/food/${id}`);
}

function getFoodFromId(id: number) {
  return http.get(`/food/${id}`);
}

export default { getFoods, saveFood, updateFood, deleteFood, getFoodFromId };
